﻿<?xml version="1.0" encoding="utf-8"?>
<CodeSnippets>
  <CodeSnippet Format="1.0.0">
    <Header>
      <Title>zz_dist_errors</Title>
      <Shortcut>zz_dist_errors</Shortcut>
      <Description>Run this in the distribution database to see the errors for each distribution agent.</Description>
      <Author />
      <SnippetTypes>
        <SnippetType>Expansion</SnippetType>
      </SnippetTypes>
    </Header>
    <Snippet>
      <Declarations />
      <Code Language="sql"><![CDATA[SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

WITH    agent_outcomes
          AS ( SELECT   agent_id ,
                        last_success = MAX(CASE WHEN runstatus = 2 THEN time
                                                ELSE CAST(0 AS DATETIME)
                                           END) ,
                        last_failure = MAX(CASE WHEN runstatus = 6 THEN time
                                           END) ,
                        last_error_id = MAX(error_id)
               FROM     dbo.MSdistribution_history AS hist
               GROUP BY agent_id
             ),
        failed_agents
          AS ( SELECT   *
               FROM     agent_outcomes
               WHERE    last_failure >= last_success
                        AND last_error_id <> 0
             ),
        agent_info
          AS ( SELECT   agent_id = id ,
                        publisher_db ,
                        publication ,
                        subscriber_db ,
                        publisher_name = ( SELECT   name
                                           FROM     sys.servers
                                           WHERE    server_id = publisher_id
                                         ) ,
                        subscriber_name = ( SELECT  name
                                            FROM    sys.servers
                                            WHERE   server_id = subscriber_id
                                          ) ,
                        subscription_type_desc = CASE subscription_type
                                                   WHEN 0 THEN 'push'
                                                   WHEN 1 THEN 'pull'
                                                 END
               FROM     dbo.MSdistribution_agents
               WHERE    subscriber_db <> N'virtual'
             )
    SELECT  inf.agent_id ,
            inf.publisher_name ,
            inf.publication ,
            inf.publisher_db ,
            inf.subscriber_name ,
            inf.subscriber_db ,
            inf.subscription_type_desc ,
            agt.last_success ,
            agt.last_failure ,
            err.id,
            last_error_code = err.error_code ,
            last_error_text = err.error_text
    FROM    failed_agents AS agt
            INNER JOIN dbo.MSrepl_errors AS err ON ( agt.last_error_id = id )
            INNER JOIN agent_info AS inf ON ( inf.agent_id = agt.agent_id )
    ORDER BY publisher_name ,
            publication ,
            last_failure;]]></Code>
    </Snippet>
  </CodeSnippet>
</CodeSnippets>