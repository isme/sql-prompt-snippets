# SQL Prompt Snippets #

SQL Server administration is easier when you script the long and complex tasks.

[Red Gate SQL Prompt](https://www.red-gate.com/products/sql-development/sql-prompt/) has a snippet manager that can recall scripts with keyboard shortcuts, right there in your query window.

This repo is a collection of scripts, formatted for use with SQL Prompt, that make me a more productive DBA.

Now you can benefit too!

### How do I get set up? ###

Install SQL Server Management Studio and Red Gate SQL Prompt.

Create a subfolder in your snippets folder called "zz_snippets".

The default snippets location is `"%LOCALAPPDATA%\Red Gate\SQL Prompt 6\Snippets"`.

Clone the repo into the new subfolder.

Open the Snippet Manager to load the new snippets.

Open a new query window.

Type "zz".

Browse the new snippets!

### What snippets are there? ###

All the snippets begin "zz" to avoid colisions with any existing snippet names you might have.

The following subsections describe each snippet.

#### zz_all_database_files ####

Creates a temp table called #all_database_files. It's like sys.master_files, but with useful extra columns.

The space_used column tells you how many pages are used within each database file. It is impossible to tell using sys.master_files alone.

The database_name column saves you having to type out DB_NAME to get the name of the database.

The snippet aggregates all the rows from sys.database\_files from each database into the temp table, and calls intrinsic functions to get the missing information for each file.

#### zz_sql_logins ####

Like sys.server_principals, but only shows the names the names of the non-MS-shipped SQL logins.

This is useful during a security audit to check that all your SQL logins have appropriate settings.

#### zz_database_permissions ####

Like sys.database_permissions, but with some of the columns renamed to be more user-friendly.

This is useful during a security audit to check which permissions a user or role has in a database.

### Notes ###

Git can clone only into an empty folder, so we create a subfolder in the main snippets folder.

This works because the Snippet Manager looks in all the subfolders for snippets files.

The Snippet Manager creates new snippets in the main snippets folder. If you want to add them to this repository, move the file into the subfolder first so that it is visible to Git. You may need to close and reopen the snippets manager as well to refresh its view of the snippets (not tested yet).

### Got a question about these scripts? ###

Create an issue in the [Issue Tracker](https://bitbucket.org/isme/sql-prompt-snippets/issues?status=new&status=open) and I'll be happy to help.

### Disclaimer ###

Please don't run any of these scripts on an important system without understanding exactly what they do.