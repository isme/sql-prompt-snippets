﻿function Get-Snippet {
  param (
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)]
    [String[]] $Path
  )

  foreach ($File in $Path) {
    $Doc = [Xml] (Get-Content $File -Raw)

    [PSCustomObject] @{
      Title = $Doc.CodeSnippets.CodeSnippet.Header.Title
      Description = $Doc.CodeSnippets.CodeSnippet.Header.Description
      Code = $Doc.CodeSnippets.CodeSnippet.Snippet.Code.'#cdata-section'
    }
  }
}


function New-ScriptContent {
  param (
    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [String]
    $Title,

    [String]
    $Description,

    [Parameter(Mandatory=$True)]
    [ValidateNotNullOrEmpty()]
    [String]
    $Code
  )

@"
-- Title: $Title
-- Description: $Description
-- 
-- Converted automatically from SQL Prompt snippet format.


$Code
"@
}


function New-Script {
  param (
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)]
    [PSCustomObject[]]
    $Snippet
  )

  process {
    foreach ($Snip in $Snippet) {
      $Path = "$($Snip.Title).sql"
    
      $ScriptContent = (
        New-ScriptContent `
          -Title $Snip.Title `
          -Description $Snip.Description `
          -Code $Snip.Code
      )
      
      Set-Content `
        -Path $Path `
        -Value $ScriptContent `
        -Encoding Unicode
    }
  }
}


function ConvertTo-SqlScript {
  param (
    [Parameter(Mandatory=$True, ValueFromPipeline=$True)]
    [String[]] $Snippet
  )

  process {
    foreach ($Snip in $Snippet) {
      Get-Snippet $Snip | New-Script
    }
  }
}



$SnippetFiles = Get-ChildItem -Path *.sqlpromptsnippet
$SnippetFiles | ConvertTo-SqlScript
