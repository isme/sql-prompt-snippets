﻿<?xml version="1.0" encoding="utf-8"?>
<CodeSnippets>
  <CodeSnippet Format="1.0.0">
    <Header>
      <Title>zz_dist_inactive_subscriptions</Title>
      <Shortcut>zz_dist_inactive_subscriptions</Shortcut>
      <Description>Run this at the distributor to find inactive subscriptions.</Description>
      <Author />
      <SnippetTypes>
        <SnippetType>Expansion</SnippetType>
      </SnippetTypes>
    </Header>
    <Snippet>
      <Declarations />
      <Code Language="sql"><![CDATA[SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

IF OBJECT_ID(N'tempdb..#inactive_subscriptions') IS NOT NULL
    BEGIN
        DROP TABLE #inactive_subscriptions;
    END;

WITH    sub_hist
          AS ( SELECT   pubservs.name AS publisher_name ,
                        agents.publication ,
                        agents.publisher_db ,
                        agents.subscriber_db ,
                        subservs.name AS subscriber_name ,
                        err.time ,
                        error_code ,
                        last_status = ( SELECT TOP ( 1 )
                                                runstatus
                                        FROM    dbo.MSdistribution_history
                                        WHERE   agent_id = hist.agent_id
                                        ORDER BY timestamp DESC
                                      ) ,
                        row_num = ROW_NUMBER() OVER ( PARTITION BY agent_id ORDER BY timestamp DESC )
               FROM     dbo.MSrepl_errors AS err
                        INNER JOIN dbo.MSdistribution_history AS hist ON ( err.id = hist.error_id )
                        INNER JOIN dbo.MSdistribution_agents AS agents ON ( agents.id = hist.agent_id )
                        INNER JOIN sys.servers AS subservs ON ( subservs.server_id = agents.subscriber_id )
                        INNER JOIN sys.servers AS pubservs ON ( pubservs.server_id = agents.publisher_id )
             ),
        inactive_subs
          AS ( SELECT   publisher_name ,
                        publication ,
                        publisher_db ,
                        subscriber_db ,
                        subscriber_name
               FROM     sub_hist
               WHERE    error_code = N'21074' -- has been marked as inactive
                        AND last_status = 6 -- is currently failed
                        AND row_num = 1 --most recent row only
             )
    SELECT  *
    INTO    #inactive_subscriptions
    FROM    inactive_subs;

SELECT  * ,
        reinitialization_command = N'
EXECUTE ' + QUOTENAME(publisher_db) + N'.sys.sp_reinitsubscription
  @publication = ' + QUOTENAME(publication, N'''') + N',
  @subscriber = ' + QUOTENAME(subscriber_name, N'''') + N',
  @destination_db = ' + QUOTENAME(subscriber_db, N'''') + N';

EXECUTE ' + QUOTENAME(publisher_db) + N'.sys.sp_startpushsubscription_agent
  @publication = ' + QUOTENAME(publication, N'''') + N',
  @subscriber = ' + QUOTENAME(subscriber_name, N'''') + N',
  @subscriber_db = ' + QUOTENAME(subscriber_db, N'''') + N';
'
FROM    #inactive_subscriptions
ORDER BY publisher_name ,
        publication ,
        subscriber_name ,
        subscriber_db;
]]></Code>
    </Snippet>
  </CodeSnippet>
</CodeSnippets>